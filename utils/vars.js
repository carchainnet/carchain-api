var path = require("path");
require("dotenv-safe").config({
  path: path.join(__dirname, "../.env"),
  sample: path.join(__dirname, "../.env.example"),
});
const networks = require("../network.js");
const env = process.env.ENV;

module.exports = {
  env,
  privateKey: process.env.PRIVATE_KEY_PROD,
  network: networks[env],
};
