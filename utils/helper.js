const HDWalletProvider = require("@truffle/hdwallet-provider");
const Web3 = require("web3");

// network: netowrk config
// privateKey: Private key of the account that makes the calls
const { env, network, privateKey } = require("./vars");

let accounts = [];

// const params = {
//   from: account,
//   gas: web3.utils.toHex(800000),
//   gasPrice: web3.utils.toHex(web3.utils.toWei("30", "gwei")),
// };

// httpProviderAddress: Address of the Http Provider
// This provider is used to make calls and send transactions to the smart contract
// webSocketProviderAddress: Address of the Web Socket Provider
// This provider is used to subcribe on the events emitted from the smart contract
const { httpProviderAddress, webSocketProviderAddress } = network;

// Http provder that signs the transactions
const providerHttp =
  env == "development"
    ? new Web3.providers.HttpProvider(httpProviderAddress)
    : new HDWalletProvider(privateKey, httpProviderAddress, 0, 1, false);

// Web socket provder that subsribes on eventss
const providerWS = new Web3.providers.WebsocketProvider(
  webSocketProviderAddress
);

// Web3 that uses http provider
const web3 = new Web3(providerHttp);
// Web3 that uses web socket provider
const web3WS = new Web3(providerWS);

/**
 * @description Returns accounts using the HDWallet Provider
 * @return accounts
 */
const getAccounts = async () => {
  if (!accounts.length) accounts = await web3.eth.getAccounts();
  return accounts;
};

/**
 * @description Creates and returns a contract instance
 * This instance is used to make calls and send transactions
 * @param {object} abi ABI of the smart contrcat
 * @param {string} contractAddress address of the deployed smart contract
 * @return contract instance
 */
const createContractInstance = async (abi, contractAddress) => {
  const accounts = await getAccounts();
  return new web3.eth.Contract(abi, contractAddress, {
    from: accounts[0],
    gas: 3000000,
  });
};
/**
 * @description Creates and returns a web socket contract instance
 * This instance is used to subscribe on events
 * @param {object} abi ABI of the smart contrcat
 * @param {string} contractAddress address of the deployed smart contract
 * @return contract instance
 */
const createContractInstanceWS = async (abi, contractAddress) => {
  return new web3WS.eth.Contract(abi, contractAddress);
};

const stop = () => {
  provider.engine.stop();
};

/**
 * @description Sets the default account for web3
 */
const setDefaultAccount = async () => {
  accounts = await getAccounts();
  web3.eth.defaultAccount = accounts[0];
};

/**
 * @description returns web3 object
 */
const getWeb3 = () => web3;
/**
 * @description returns web3 object which uses web socket
 */
const getWeb3WS = () => web3WS;

const StringToFixedBytes = (asciiString, sizeInBytes) => {
  let hexString = web3.utils.toHex(asciiString);
  const sizeInHex = sizeInBytes * 2;
  const currentSize = hexString.length - 2; // without 0x
  const emptySpace = sizeInHex - currentSize;
  if (emptySpace < 0) hexString = hexString.sub(0, 33);
  else hexString += "0".repeat(emptySpace);
  return hexString;
};

module.exports = {
  getAccounts,
  createContractInstance,
  createContractInstanceWS,
  stop,
  setDefaultAccount,
  getWeb3,
  getWeb3WS,
  StringToFixedBytes,
};

// rinkeby addresses

// ERC20Capped 0x708563081D7f52bc1Afd5a490b74143Ac03f0D4e
// Assigner 0x66C0EC754a381e7A9A1f5bC6F6CB8fcb152295eE
// ServiceProvider 0x6dc5ca8b45bcff851A60Bb053Ec597d12003BD75
