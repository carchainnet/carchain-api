const infuraProjectId = "5bf20355f34e486b974de798fe4bf7f0";

module.exports = {
  development: {
    httpProviderAddress: "http://localhost:8545", // Localhost (default: none)
    webSocketProviderAddress: "ws://localhost:8545", // Localhost (default: none)
    contractAddresses: {
      Chainlink: "0x436d823ff4a5A80229951476f44bCA57b7e751de",
      MinimeToken: "0x553ab21E3b611a957E87a06048De928F189a9436",
      Assigner: "0xa2da56e4e159272B8362035b70E5F798826424F7",
      ServiceProvider: "0xbfeCF245bB737a404c46edfaC91Ffa89971fC451",
      DriverManager: "0xCe009ee5Bc4c2c10A14907f266f2b1312a5E0275",
    },
  },
  rinkeby: {
    httpProviderAddress: "https://rinkeby.infura.io/v3/" + infuraProjectId, // Localhost (default: none)
    webSocketProviderAddress:
      "wss://rinkeby.infura.io/ws/v3/" + infuraProjectId, // Localhost (default: none)
    contractAddresses: {
      Chainlink: "0xEdbe8565eE32d6384159f8f8452faeeAe2Fb6158",
      MinimeToken: "0x2d5e0FaC3A1320b9d0B9cCe39aa376842cAe1395",
      Assigner: "0x7C07A1c8F3b7e1607c4B10B4840395d6C77908ac",
      ServiceProvider: "0xF7Df073DcA6C2903F1616418a00CE14bF71c98Ba",
      DriverManager: "0xEcdD8cF34711A7B5db9F904D9039f81DCa50e215",
    },
  },
};
