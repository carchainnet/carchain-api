# Carchain API

## Usage
clone the repository, and run:
```bash
npm install
```
Then run:
```bash
cp .env.example .env
```
Insert your environment variables into .env, and your contracts information into network.js.

## Example Scenario
```bash
node examples/example.js
```

