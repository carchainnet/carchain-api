const Assigner = require("../api/Assigner");
const ServiceProvider = require("../api/ServiceProvider");
const DriverManager = require("../api/DriverManager");
const Chainlink = require("../api/Chainlink");
const Token = require("../api/Token");
const { getAccounts, getWeb3 } = require("../utils/helper.js");
const { network } = require("../utils/vars");

const assignerContractAddress = network.contractAddresses.Assigner;
const SPContractAddress = network.contractAddresses.ServiceProvider;

const firstIp = "0x2e69a38d000000000000000000000000";
const secondIp = "0x12000000000000000000000000000000";
const thirdIp = "0x13000000000000000000000000000000";
const firstName = "First SP";
const secondName = "Second SP";
const thirdName = "Third SP";
const city1 = "Tehran8";
const city2 = "Isfahan8";
const city3 = "Shiraz8";
const country = "Iran8";
const ipfsAddress1 =
  "1220795d2d620fcff3f3a71b706513aaf2063e40f982243e567a2013cc51244c871d";

const ipfsAddress2 =
  "12204ee6142e0381583cf7efe0bb6833d2a435da811bf1bb93bd3b3709c9720ba5b8";

const main = async () => {
  const chainlink = await Chainlink.build();
  // Request to update the token price on blockchain and wait until
  // the price is update (could take some blocks).
  console.log("Updating Price");
  await chainlink.requestPriceAndWaitForUpdate();
  console.log("Price Update Finished");
  const accounts = await getAccounts(); // Get available accounts
  const token = await Token.build(); // Token contract instance
  const assigner = await Assigner.build(); // Assigner contract instance
  const serviceProvider = await ServiceProvider.build(); // Service provider contract instance
  const driverManager = await DriverManager.build(); // Driver manager contract instance
  await token.mint(accounts[0], 10); // Mint 10 tokens for account 0
  // Transfer some tokens to another account
  await token.transfer("0x5A3384734f1305aA2b759B02DbB727AAbaC040Cf", 5);

  console.log("Add City To Assigners");
  await assigner.addCountry(country); // Add a country
  await assigner.addCity(country, city1); // Add a city1
  await assigner.addCity(country, city2); // Add a city2
  // Due to security concerns
  // if an account has alraedy been approved some value
  // bofore setting a new value The approve value shoud be set 0
  await token.approve(assignerContractAddress, 0);
  // Approve 10 toeken to `assignerContractAddress`
  // Meaning assignerContractAddress can spend up to
  // 10 tokens from the account of the `msg.sender`
  await token.approve(assignerContractAddress, 10);

  console.log("Adding Assigners");
  await assigner.addAssigner(country, city1, firstIp); //Add an assigner to city1
  await assigner.addAssigner(country, city1, secondIp); //Add another assigner to city1
  await assigner.removeAssigner(country, city1, firstIp); //delete an assigner from city1
  const assigners = await assigner.getAssignersInCity(country, city1, 0, -1); // get assigners in city1
  console.log(assigners);

  console.log("Add City To Service Providers");
  await serviceProvider.addCountry(country); // Add a country
  await serviceProvider.addCity(country, city1); // Add a city1
  await serviceProvider.addCity(country, city2); // Add a city1
  await serviceProvider.addCity(country, city3); // Add a city3
  // Due to security concerns
  // if an account has alraedy been approved some value
  // bofore setting a new value The approved value shoud be set 0
  await token.approve(SPContractAddress, 0);
  await token.approve(SPContractAddress, 10); // Approve 10 toeken to `SPContractAddress`

  console.log("Adding Service Providers");
  await serviceProvider.addServiceProvider(country, city1, firstIp, firstName); // Add a service provider to city1
  await serviceProvider.addServiceProvider(
    country,
    city2,
    secondIp,
    secondName
  ); // Add a service provider to city2
  await serviceProvider.addServiceProvider(country, city3, thirdIp, thirdName); // Add a service provider to city3
  await serviceProvider.addServiceProvider(
    country,
    city1,
    secondIp,
    secondName
  ); // Add a service provider to city1
  await serviceProvider.removeServiceProvider(country, city1, firstIp); // remove a service provider from city1
  const SPs = await serviceProvider.getSPsInCity(country, city1, 0, -1); // get service providers in city1
  console.log(SPs);

  console.log("Adding Drivers");
  // prasing a ipfs address into it's multihash components
  const {
    hashFunction: hashFunction1,
    hashSize: hashSize1,
    hashValue: hashValue1,
  } = DriverManager.parseMultiHashFromHexString(ipfsAddress1);

  const {
    hashFunction: hashFunction2,
    hashSize: hashSize2,
    hashValue: hashValue2,
  } = DriverManager.parseMultiHashFromHexString(ipfsAddress2);

  // Adding a driver

  await driverManager.addDriver(
    country,
    city1,
    firstIp,
    hashFunction1,
    hashSize1,
    hashValue1
  );

  // Adding a driver
  await driverManager.addDriver(
    country,
    city1,
    firstIp,
    hashFunction2,
    hashSize2,
    hashValue2
  );

  // Adding a driver
  await driverManager.addDriver(
    country,
    city2,
    secondIp,
    hashFunction1,
    hashSize1,
    hashValue1
  );

  // Adding a driver
  await driverManager.updateDriver(
    country,
    city1,
    firstIp,
    hashFunction1,
    hashSize1,
    hashValue1
  );

  // Adding a driver
  await driverManager.removeDriver(
    country,
    city1,
    firstIp,
    hashFunction1,
    hashSize1,
    hashValue1
  );

  // Getting hash changes for drivers of sp corresponsing to `country`, `city1`, `firstIp`
  const driversForSp = await driverManager.getDriversHashChanges(
    country,
    city1,
    firstIp,
    0,
    -1
  );

  console.log(driversForSp);
  console.log("First Change:", driversForSp[0][0]);
  console.log("Hash Function:", driversForSp[1][0]);
  console.log("Hash Size:", driversForSp[2][0]);
  console.log("Hash Value:", driversForSp[3][0]);

  // Subsribing on AddDriver events in the driver manager smart contract
  // The events are filtered by countryName, cityName
  const subscription = driverManager.subscribeOnChanges(
    "AddDriver",
    country,
    city1,
    0,
    (err, event) => {
      if (err) console.log(err);
      else console.log("Driver Added to sp with ip", event.returnValues.ip);
    }
  );

  // Subsribing on AddAssigenr events in the assigner smart contract
  const subscription1 = assigner.subscribeOnEvent(
    "AddAssigner",
    {
      fromBlock: 0,
    },
    (err, event) => {
      if (err) console.log(err);
      else console.log("Add Assigner Event:", event);
    },
    0
  );

  console.log("Finisehd");
  return;
};

main().catch((err) => console.log(err));
