const contrcat = require("../build/contracts/Assigner.json");
const ContractBase = require("./ContractBase");
const ContractBuilder = require("./ContractBuilder");
const { network } = require("../utils/vars");
const { StringToFixedBytes } = require("../utils/helper.js");

const { abi } = contrcat;

const contractAddress = network.contractAddresses.Assigner;

/** Assigner Class to interact with the assigner smart contract.
 * This class is singleton.
 * @extends ContractBase
 */
class Assigner extends ContractBase {
  /** Build the singleton instance of class
   * returns the existing instance if already created
   */
  static async build() {
    return ContractBuilder.build(Assigner, abi, contractAddress);
  }

  /**
   * @description adds `countryName` to the countries;
   *
   *
   * <br>Emits an {AddCountry} event.
   *
   * <br> Requirements:
   *
   * <br>&nbsp;- `countryName` should be unique.
   *
   * @param {string} countryName Name of the country to be addded
   */
  async addCountry(countryName) {
    //   const accounts = await getAccounts();
    const hexCountry = StringToFixedBytes(countryName, 20);
    return await this.contractInstance.methods.addCountry(hexCountry).send();
  }

  /**
   * @description adds `cityName` to the countries;
   *
   *
   * <br>Emits an {AddCity} event.
   *
   * <br> Requirements:
   *
   * <br>&nbsp;- `countryName` should be already added using {addCountry}.
   * <br>&nbsp;- `cityName` should be unique in the country.
   *
   * @param {string} countryName Name of the country of the city
   * @param {string} cityName Name of the city to be addded
   *
   */
  async addCity(countryName, cityName) {
    //   const accounts = await getAccounts();
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .addCity(hexCountry, hexCity)
      .send();
  }

  /**
   * @async
   * @description
   * Adds assigner with ip `ip` to city `cityName`;
   *
   * <br>Emits an {AddAssigner} event.
   *
   * <br>Requirements:
   *    <br>&nbsp;- `cityName` should have been already added.
   *    <br>&nbsp;- `ip` should be unique in the city.
   *    <br>&nbsp;- `msg sender` should have approved enough toknes to be deposited
   *    <br>&nbsp;- The token price on blockchain should be updated at most 5 minutes ago
   * @see {@link Chainlink}
   *
   * @param {string} countryName Name of the country to add the assigner
   * @param {string} cityName Name of the city to add the assigner
   * @param {IP} ip Ip of the assigner to be added.
   */
  async addAssigner(countryName, cityName, ip) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .addAssigner(hexCountry, hexCity, ip)
      .send();
  }

  /**
   * @description Removes assigner with ip `ip` from city `cityName`;
   *
   * <br>Emits an {RemoveAssigner} event.
   *
   * <br> Requirements:
   *
   * <br>&nbsp;- `cityName` should be added.
   * <br>&nbsp;- `ip` should be added to the city.
   *
   * @param {string} countryName Name of the country to remove the assigner from
   * @param {string} cityName Name of the city to remove the assigner from
   * @param {IP} ip Ip of the assigner to be removed
   */
  async removeAssigner(countryName, cityName, ip) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .removeAssigner(hexCountry, hexCity, ip)
      .send();
  }

  /**
   * @description Assigner with ip `ip` inside city `cityName`
   * withdraws his locked deposit;
   * the assingner should call {removeAssignerFromRegion},
   * and wait a centain amount of time calling this method.
   *
   * <br>Emits an {WithdrawLockedDeposit} event indicating the withdrawal.
   *
   * <br>Requirements:
   *
   * <br>&nbsp;- `ip` should have been removed from `cityName` at least 2 minutes ago
   *
   * @param {string} countryName Name of the country of the assigner
   * @param {string} cityName Name of the city of the assigner
   * @param {IP} ip Ip of the assigner
   */
  async withdrawLockedDeposit(countryName, cityName, ip) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .withdrawLockedDeposit(hexCountry, hexCity, ip)
      .send();
  }

  /**
   * @description Returns the ips inside country `countryName` and city `cityName`
   * @param {stirng} countryName Name of the country
   * @param {stirng} cityName Name of the city
   * @param startIndex start index of array
   * @param endIndex end index of array (-1 for last index of the array)
   * @return {IP[]} array of the ips inside the city
   */
  async getAssignersInCity(countryName, cityName, startIndex, endIndex) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .getAssignersInCity(hexCountry, hexCity, startIndex, endIndex)
      .call();
  }
}

module.exports = Assigner;
