// 12 word mnemonic for HD Wallet Provider
// You can use any provider such as the HttpProvider if you are
// signing with private key

const contrcat = require("../build/contracts/MinimeToken.json");
const { network } = require("../utils/vars");
const ContractBase = require("./ContractBase");
const ContractBuilder = require("./ContractBuilder");

const { abi } = contrcat;
const contractAddress = network.contractAddresses.MinimeToken;

/** Assigner Class to interact with the token smart contract.
 * This class is singleton.
 * @extends ContractBase
 */
class Token extends ContractBase {
  /** Build the singleton instance of class
   * returns the existing instance if already created
   */
  static async build() {
    return ContractBuilder.build(Token, abi, contractAddress);
  }

  /**
   * @description Returns balance of `account`
   * @param {stirng} account account number of user
   * @return {number} balance of user
   */
  async getBalance(account) {
    return await this.contractInstance.methods.balanceOf(account).call();
  }

  /**
   * @description Moves `amount` tokens from the caller's account to `recipient`.
   *
   *  <br>Emits a {Transfer} event.
   *
   * @param {number} amonunt amount of tokens to tranfer
   * @param {string} recipient recipient of the tokens
   *
   */
  async transfer(amount, recipient) {
    //   const accounts = await getAccounts();
    return await this.contractInstance.methods
      .transfer(amount, recipient)
      .send();
  }

  /**
   * @description Returns the remaining number of tokens that `spender` will be
   * allowed to spend on behalf of `owner` through {transferFrom}. This is
   * zero by default.
   *
   * <br>This value changes when {approve} or {transferFrom} are called.
   *
   * @param {string} owner address of the token
   * @param {string} spender address of user who is allowed to spend
   *
   */
  async getAllowance(owner, spender) {
    return await this.contractInstance.methods.allowance(owner, spender).call();
  }

  /**
   * @description Sets `amount` as the allowance of `spender` over the caller's tokens.
   *
   * <br> Due to security concerns
   * <br> if an account has alraedy been approved some value
   * <br> bofore setting a new value The approved value shoud be set 0
   *
   *  <br>Emits an {Approval} event.
   *
   * @param {string} spender address of the spender
   * @param {number} amount amount of tokens allowed
   */
  async approve(spender, amount) {
    return await this.contractInstance.methods.approve(spender, amount).send();
  }

  /**
   * @description Moves `amount` tokens from `sender` to `recipient` using the
   * allowance mechanism. `amount` is then deducted from the caller's
   * allowance.
   *
   *  <br>Emits a {Transfer} event.
   *
   * @param {string} address of the token holder
   * @param {recipient} address of the recipient
   * @param {amount} address amount tokens to transfer
   */
  async transferFrom(sender, recipient, amount) {
    return await this.contractInstance.methods
      .tranferForm(sender, recipient, amount)
      .send();
  }

  /** @description Creates `amount` tokens and assigns them to `account`, increasing
   * the total supply.
   *
   *  <br>Emits a {Transfer} event with `from` set to the zero address.
   *
   * @param {string } account the recipient of the minted tokens
   * @param {string } amount amount tokens to mint
   *
   * Requirements
   *
   * - `to` cannot be the zero address.
   * - only contract owner can call this method
   */
  async mint(account, amount) {
    return await this.contractInstance.methods
      .generateTokens(account, amount)
      .send();
  }
}

module.exports = Token;
