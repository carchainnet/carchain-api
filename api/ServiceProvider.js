const contrcat = require("../build/contracts/ServiceProvider.json");
const { network } = require("../utils/vars");
const ContractBase = require("./ContractBase");
const ContractBuilder = require("./ContractBuilder");
const { StringToFixedBytes } = require("../utils/helper.js");

const { abi } = contrcat;

const contractAddress = network.contractAddresses.ServiceProvider;

/** Assigner Class to interact with the service provider smart contract.
 * This class is singleton.
 * @extends ContractBase
 */
class ServiceProvider extends ContractBase {
  /** Build the singleton instance of class
   * returns the existing instance if already created
   */
  static async build() {
    return ContractBuilder.build(ServiceProvider, abi, contractAddress);
  }

  /**
   * @description adds `countryName` to the countries;
   *
   *
   * <br>Emits an {AddCountry} event.
   *
   * <br> Requirements:
   *
   * <br>&nbsp;- `countryName` should be unique.
   *
   * @param {string} countryName Name of the country to be addded
   */
  async addCountry(countryName) {
    //   const accounts = await getAccounts();
    const hexCountry = StringToFixedBytes(countryName, 20);
    return await this.contractInstance.methods.addCountry(hexCountry).send();
  }

  /**
   * @description adds `cityName` to the countries;
   *
   *
   * <br>Emits an {AddCity} event.
   *
   * <br> Requirements:
   *
   * <br>&nbsp;- `countryName` should be already added using {addCountry}.
   * <br>&nbsp;- `cityName` should be unique in the country.
   *
   * @param {string} countryName Name of the country of the city
   * @param {string} cityName Name of the city to be addded
   *
   */
  async addCity(countryName, cityName) {
    //   const accounts = await getAccounts();
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .addCity(hexCountry, hexCity)
      .send();
  }

  /**
   * @description Adds service provider with ip `ip` and name `name` to city `cityName`;
   *
   * <br>Emits an {AddSP} event.
   *
   * <br>Requirements:
   *    <br>&nbsp;- `cityName` should have been already added.
   *    <br>&nbsp;- `ip` should be unique in the region.
   *    <br>&nbsp;- `msg sender` should have approved enough toknes to be deposited
   *    <br>&nbsp;- The token price on blockchain should be updated at most 5 minutes ago
   *
   * @see {@link Chainlink}
   *
   * @param {string} countryName Name of the country to add the service provider
   * @param {string} cityName Name of the city to add the serive provider
   * @param {IP} ip Ip of the service provider to be added.
   * @param {string} name name of the service provider to be added.
   */
  async addServiceProvider(countryName, cityName, ip, name) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    const hexName = StringToFixedBytes(name, 20);
    return await this.contractInstance.methods
      .addSP(hexCountry, hexCity, ip, hexName)
      .send();
  }

  /**
   * @description Removes service provider with ip `ip` from city `cityName`;
   *
   * <br>Emits an {RemoveSP} event.
   *
   * <br> Requirements:
   *
   * <br>&nbsp;- `cityName` should be added.
   * <br>&nbsp;- `ip` should be added to the country.
   *
   * @param {string} countryName Name of the country to remove the service provider from
   * @param {string} cityName Name of the city to remove the service provider from
   * @param {IP} ip Ip of the service provider to be removed
   */
  async removeServiceProvider(countryName, cityName, ip) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .removeSP(hexCountry, hexCity, ip)
      .send();
  }

  /**
   * @description Service provider with ip `ip` inside city `cityName`
   * withdraws his locked deposit;
   * the assingner should call {removeSPFromCountry},
   * and wait a centain amount of time calling this method.
   *
   * <br>Emits an {WithdrawLockedDeposit} event indicating the withdrawal.
   *
   * <br>Requirements:
   *
   * <br>&nbsp;- `ip` should have been removed from `city` at least 2 minutes ago
   *
   * @param {string} countryName Name of the country of the service provider
   * @param {string} cityName Name of the city of the service provider
   * @param {IP} ip Ip of the service provider
   *
   */
  async withdrawLockedDeposit(countryName, cityName, ip) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .withdrawLockedDeposit(hexCountry, hexCity, ip)
      .send();
  }

  /**
   * @description Returns the ips inside country `countryName` and city `cityName`
   * @param {stirng} countryName Name of the country
   * @param {string} cityName Name of the city
   * @param startIndex start index of array
   * @param endIndex end index of array (-1 for last index of the array)
   * @return {IP[]} array of the ips inside the city
   */
  async getSPsInCity(countryName, cityName, startIndex, endIndex) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .getSPsInCity(hexCountry, hexCity, startIndex, endIndex)
      .call();
  }
}

module.exports = ServiceProvider;
