const { getWeb3WS } = require("../utils/helper.js");

/**
 * BN for caculating with big numbers. Use to toNumber() to convert to integer
 *
 * @typedef {Object} BN
 * @see {@link https://web3js.readthedocs.io/en/v1.2.0/web3-utils.html#bn}
 */

/**
 * an string reprenting IPv6 or IPv4
 *    <br>&nbsp;- Should have Hex Format
 *    <br>&nbsp;- Example IPv6: "0x20010db885a3000000008a2e03707334"
 *    <br>&nbsp;- Example Ipv4: "0xC00002EB"
 * @typedef {string} IP
 */

/** Base Contract Class to interact with the contracts.
 * <br> Contrct classes inherit from this class
 */
class ContractBase {
  /**
   * @description Initializes the parameters
   *
   * @param {object} _abi abi of the contrcat
   * @param {string} _contractAddress address of the contract
   * @param {object} _contractInstance contract instance created with http provider
   * @param {object} _contractInstanceWS contract instance created with web socket provider
   */
  constructor(_abi, _contractAddress, _contractInstance, _contractInstanceWS) {
    this.abi = _abi;
    this.contractAddress = _contractAddress;
    this.contractInstance = _contractInstance;
    this.contractInstanceWS = _contractInstanceWS;
  }

  /**
   * @description Returns http contrcat instance
   * @return http contract instance
   */
  getContractInstance() {
    return this.contractInstance;
  }

  /**
   * @description Returns events that have been submitted in the past
   * @param {stirng} eventName Name of the events to retrieve
   * @return array of events
   * @see {@link https://web3js.readthedocs.io/en/v1.2.0/web3-eth-contract.html#getpastevents}
   */
  async getPastEvents(eventName) {
    const events = await this.contractInstanceWS.getPastEvents(eventName, {
      fromBlock: 0,
    });
    return events;
  }

  /**
   * Callback to call when an event is emitted
   *
   * @callback subscribeOnEventCallback
   * @param {object} error - error (if any)
   * @param {object} event - the event data
   */

  /**
   * @description Subcribes on the events that will be (or have been) emitted
   * uses websocket provider
   * @param {stirng} eventName Name of the events to listen to. use allEvents to
   * @param {object} options options of the events. examples:
   * <br>&nbsp;- { fromBlock: 0 },
   * <br>&nbsp;- {filter: {myIndexedParam: [20,23], myOtherIndexedParam: '0x123456789...'},  fromBlock: 100}
   * @param {subscribeOnEventCallback} callback Callback to call when an event is emitted
   * @see {@link https://web3js.readthedocs.io/en/v1.2.0/web3-eth-contract.html#contract-events}
   */

  subscribeOnEvent(eventName, options, callback) {
    return this.contractInstanceWS.events[eventName](options, callback);
  }

  // /**
  //  * @description Subcribes on the events that will be (or have been) emitted
  //  * uses websocket provider. The difference to {subscribeOnEvent} is that can be unsubscribed.
  //  * @param {stirng} eventName Name of the events to listen to. use allEvents to
  //  * @param {subscribeOnEventCallback} callback Callback to call when an event is emitted
  //  * @see {@link https://web3js.readthedocs.io/en/v1.2.0/web3-eth-contract.html#contract-events}
  //  */
  // subscribeOnEventDirty(eventName, callback, blockNumber = 0) {
  //   const web3 = getWeb3WS();
  //   const eventJsonInterface = web3.utils._.find(
  //     this.contractInstance._jsonInterface,
  //     (o) => o.name === eventName && o.type === "event"
  //   );
  //   const subscription = web3.eth.subscribe(
  //     "logs",
  //     {
  //       address: this.contractInstance.options.address,
  //       topics: [eventJsonInterface.signature],
  //       fromBlock: blockNumber,
  //     },
  //     (error, result) => {
  //       let eventObj = {};
  //       if (!error) {
  //         eventObj = web3.eth.abi.decodeLog(
  //           eventJsonInterface.inputs,
  //           result.data,
  //           result.topics.slice(1)
  //         );
  //       }
  //       callback(error, eventObj);
  //     }
  //   );
  //   return subscription;
  // }
}

module.exports = ContractBase;
