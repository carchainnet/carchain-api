const contrcat = require("../build/contracts/DriverManager.json");
const { network } = require("../utils/vars");
const ContractBase = require("./ContractBase");
const ContractBuilder = require("./ContractBuilder");
const multihashes = require("multihashes");
const { getWeb3, StringToFixedBytes } = require("../utils/helper.js");

const { abi } = contrcat;

const contractAddress = network.contractAddresses.DriverManager;

/** Assigner Class to interact with the service provider smart contract.
 * This class is singleton.
 * @extends ContractBase
 */
class DriverManager extends ContractBase {
  /** Build the singleton instance of class
   * returns the existing instance if already created
   */
  static async build() {
    return ContractBuilder.build(DriverManager, abi, contractAddress);
  }

  /**
   * @description adds the address of a driver profile
   * on the ipfs to service provier
   * corresponsind to `country`, 'city`, 'ip`.
   * The address is a Multihash
   *
   * Emits an {AddDriver} event indicating the addition.
   *
   * Requirements:
   *
   * - `msg.sender` should have added `ip` to `countryName`, `cityName`
   *      in the service provider contract
   *
   * @param {string} countryName Name of the country of the service provider
   * @param {string} cityName Name of the city of the service provider
   * @param {IP} ip Ip of the service provider
   * @param {number} hashFunction code of the hash function of the multihash
   * @param {number} hashSize size of the multihash
   * @param {string} hashValue hash value of the multihash as hex value.
   */
  async addDriver(
    countryName,
    cityName,
    ip,
    hashFunction,
    hashSize,
    hashValue
  ) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .addDriver(hexCountry, hexCity, ip, hashFunction, hashSize, hashValue)
      .send();
  }

  /**
   * @description removes the address of a driver profile
   * on the ipfs for service provier
   * corresponsind to `country`, 'city`, 'ip`.
   * The address is a Multihash
   *
   * Emits an {RemoveDriver} event indicating the removal.
   *
   * Requirements:
   *
   * - `msg.sender` should have added `ip` to `countryName`, `cityName`
   *      in the service provider contract
   *
   * @param {string} countryName Name of the country of the service provider
   * @param {string} cityName Name of the city of the service provider
   * @param {IP} ip Ip of the service provider
   * @param {number} hashFunction code of the hash function of the multihash
   * @param {number} hashSize size of the multihash
   * @param {string} hashValue hash value of the multihash as hex value.
   */
  async removeDriver(
    countryName,
    cityName,
    ip,
    hashFunction,
    hashSize,
    hashValue
  ) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .removeDriver(hexCountry, hexCity, ip, hashFunction, hashSize, hashValue)
      .send();
  }

  /**
   * @description indicates an update has happened ot
   * the address of a driver profile
   * on the ipfs for service provier
   * corresponsind to `country`, 'city`, 'ip`.
   * The address is a Multihash
   *
   * Emits an {UpdateDriver} event indicating the removal.
   *
   * Requirements:
   *
   * - `msg.sender` should have added `ip` to `countryName`, `cityName`
   *      in the service provider contract
   *
   * @param {string} countryName Name of the country of the service provider
   * @param {string} cityName Name of the city of the service provider
   * @param {IP} ip Ip of the service provider
   * @param {number} hashFunction code of the hash function of the multihash
   * @param {number} hashSize size of the multihash
   * @param {string} hashValue hash value of the multihash as hex value.
   */
  async updateDriver(
    countryName,
    cityName,
    ip,
    hashFunction,
    hashSize,
    hashValue
  ) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .updateDriver(hexCountry, hexCity, ip, hashFunction, hashSize, hashValue)
      .send();
  }

  /**
   * An object representing hash changes. Hash change coontins two parts:
   * <br> (1) the change: which can have values 0:added, 1: deleted, 2:updated.
   * <br> (2) a multihash @see {@link https://github.com/multiformats/multihash}
   * <br> The hash change is divided into four arrays to be able to return them from solidity. One array indicates the changes and three arrays to represent the multihash.
   *
   * @typedef {Object} HashChangeList
   * @property {string[]} 0 - array of changes.
   * @property {string[]} 1 - array indicating codes of hash functions of multihashes.
   * @property {string[]} 2 - array indicating hash sizes of multihashes.
   * @property {string[]} 3 - array indicating hash values as 32-bit hashes.
   */

  /**
   * @description Returns the change list
   * in the provided range of the array for
   * drivers beloging to service provider
   * corresponsind to `country`, 'city`, 'ip`.
   * @param {string} countryName  Name of the country of the service provider
   * @param {string} cityName Name of the city of the service provider
   * @param {IP} ip Ip of the service provider
   * @param {number} startIndex start index of array
   * @param {number} endIndex end index of array. set as -1 to indicate last index.
   * @return {HashChangeList} representing the changes to the drivers of an assigner
   */
  async getDriversHashChanges(countryName, cityName, ip, startIndex, endIndex) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .getDriversHashChanges(hexCountry, hexCity, ip, startIndex, endIndex)
      .call();
  }

  /**
   * @description Returns hash functions of Multihashes
   * in the provided range of the array for
   * drivers beloging to service provider
   * corresponsind to `country`, 'city`, 'ip`.
   * @param {string} countryName  Name of the country of the service provider
   * @param {string} cityName Name of the city of the service provider
   * @param {IP} ip Ip of the service provider
   * @param {number} startIndex start index of array
   * @param {number} endIndex end index of array. set as -1 to indicate last index.
   * @return {string[]} array of hash functions
   */
  async getDriversHashFunctions(
    countryName,
    cityName,
    ip,
    startIndex,
    endIndex
  ) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .getDriversHashFunctions(hexCountry, hexCity, ip, startIndex, endIndex)
      .call();
  }

  /**
   * @description Returns hash sizes of Multihashes
   * in the provided range of the array for
   * drivers beloging to service provider
   * corresponsind to `country`, 'city`, 'ip`.
   * @param {string} countryName  Name of the country of the service provider
   * @param {string} cityName Name of the city of the service provider
   * @param {IP} ip Ip of the service provider
   * @param {number} startIndex start index of array
   * @param {number} endIndex end index of array. set as -1 to indicate last index.
   * @return {string[]} array of hash sizes.
   */
  async getDriversHashSizes(countryName, cityName, ip, startIndex, endIndex) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .getDriversHashSizes(hexCountry, hexCity, ip, startIndex, endIndex)
      .call();
  }

  /**
   * @description Returns hash values of Multihashes
   * in the provided range of the array for
   * drivers beloging to service provider
   * corresponsind to `country`, 'city`, 'ip`.
   * @param {string} countryName  Name of the country of the service provider
   * @param {string} cityName Name of the city of the service provider
   * @param {IP} ip Ip of the service provider
   * @param {number} startIndex start index of array
   * @param {number} endIndex end index of array. set as -1 to indicate last index.
   * @return {string[]} array of hash values
   */
  async getDriversHashValues(countryName, cityName, ip, startIndex, endIndex) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .getDriversHashValues(hexCountry, hexCity, ip, startIndex, endIndex)
      .call();
  }

  /**
   * @description Returns hash values of Multihashes
   * in the provided range of the array for
   * drivers beloging to service provider
   * corresponsind to `country`, 'city`, 'ip`.
   * @param {string} countryName  Name of the country of the service provider
   * @param {string} cityName Name of the city of the service provider
   * @param {IP} ip Ip of the service provider
   * @param {number} startIndex start index of array
   * @param {number} endIndex end index of array. set as -1 to indicate last index.
   * @return {string[]} array of hash values
   */
  async getDriversChanges(countryName, cityName, ip, startIndex, endIndex) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    return await this.contractInstance.methods
      .getDriversChanges(hexCountry, hexCity, ip, startIndex, endIndex)
      .call();
  }

  /**
   * Callback to call when an event is emitted
   *
   * @callback subscribeOnEventCallback
   * @param {object} error - error (if any)
   * @param {object} event - the event data
   */

  /**
   * @description Subcribes on the chanegs events that will be (or have been) emitted
   * The events are filtered by `countryName` and `cityName`.
   * @param {stirng} eventName Name of the events to listen to. use allEvents to
   * @param {string} countryName  Name of the country of the service provider
   * @param {string} cityName Name of the city of the service provider
   * @param {number} blockNumber the number of the start block
   * @param {subscribeOnEventCallback} callback Callback to call when an event is emitted
   */

  subscribeOnChanges(eventName, countryName, cityName, blockNumber, callback) {
    const hexCountry = StringToFixedBytes(countryName, 20);
    const hexCity = StringToFixedBytes(cityName, 20);
    const options = {
      fromBlock: blockNumber,
      filter: {
        countryName: hexCountry,
        cityName: hexCity,
      },
      // topics: [
      //   undefined,
      //   web3.utils.sha3(hexCountry),
      //   web3.utils.sha3(hexCity),
      // ],
    };
    return this.subscribeOnEvent(eventName, options, callback);
  }

  /**
   * @see {@link https://multiformats.github.io/js-multihash/#multihash}
   *
   * @typedef {object} multihash
   */

  /**
   * @static
   * @description converts a base 58 hash string into a multi hash
   * @param {string} hashString the hash string
   * @return {multihash} multihash object
   */
  static parseMultiHashFromB58String(hashString) {
    const multihash = multihashes.fromB58String(hashString);
    return DriverManager.parseMutltiHash(multihash);
  }

  /**
   * @static
   * @description converts a hex hash string into a multi hash
   * @param {string} hashString the hash string
   * @return {multihash} multihash object
   */
  static parseMultiHashFromHexString(hashString) {
    const multihash = multihashes.fromHexString(hashString);
    return DriverManager.parseMutltiHash(multihash);
  }

  /**
   * @static
   * @description parses a multi hash and returns its parts
   * @param {multihash} multihash the multihash to be parsed
   * @return {object} multihash parts including hashFunction, hashSize, hashValue.
   */
  static parseMutltiHash(multihash) {
    const decodedHash = multihashes.decode(multihash);
    const hashFunction = decodedHash.code;
    const hashSize = decodedHash.length;
    const hashValue = "0x" + multihashes.toHexString(decodedHash.digest);
    return {
      hashFunction,
      hashSize,
      hashValue,
    };
  }
}

module.exports = DriverManager;
