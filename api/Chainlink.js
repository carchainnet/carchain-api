// 12 word mnemonic for HD Wallet Provider
// You can use any provider such as the HttpProvider if you are
// signing with private key

const contrcat = require("../build/contracts/IChainlinkClient.json");
const { network } = require("../utils/vars");
const ContractBase = require("./ContractBase");
const ContractBuilder = require("./ContractBuilder");

const { abi } = contrcat;
const contractAddress = network.contractAddresses.Chainlink;

/** Assigner Class to interact with the token smart contract.
 * This class is singleton.
 * @extends ContractBase
 */
class Chainlink extends ContractBase {
  /** Build the singleton instance of class
   * returns the existing instance if already created
   */
  static async build() {
    return ContractBuilder.build(Chainlink, abi, contractAddress);
  }

  /**
   * @description Requests an update to the price of token on blockchain
   * <b> This request will be fulfilled in the subsequent blocks
   * <b> When the request is fulfilled a {NewPrice} will be emitted.
   */
  async requestTokenPrice() {
    return await this.contractInstance.methods.requestTokenPrice().send();
  }

  /**
   * @description Returns the last time the price was updated
   * @return {number} date of last update
   */
  async getLastTime() {
    return await this.contractInstance.methods.getLastTime().call();
  }

  /**
   * @description Returns the last price
   * @return {number} the last price
   */
  async getCurrentPrice() {
    return await this.contractInstance.methods.getCurrentPrice().call();
  }

  /**
   * @description Sends a request to update the price.
   * <br> waits till the price is updated (might take some blocks because of chailink mechanism)
   * <br> is resolved when the price is update.
   * @return {object} the event object indicating the price was updated.
   * <br> the events contain the last price and last update time
   */
  requestPriceAndWaitForUpdate() {
    return new Promise((resolve, reject) => {
      this.requestTokenPrice().then((res, err) => {
        const { blockNumber } = res;
        console.log(err)

        const subscription = this.subscribeOnEvent(
          "NewPrice",
          { fromBlock: blockNumber },
          (err, event) => {
            if (err) console.log(err);
            subscription.unsubscribe(function (error, success) {
              if (success) console.log("Successfully unsubscribed!");
            });
            resolve(event);
          }
        );
      });
    });
  }
}

module.exports = Chainlink;
