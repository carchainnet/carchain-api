const {
  createContractInstance,
  createContractInstanceWS,
} = require("../utils/helper.js");

/**
 * Builder of the signleton contrcat classes
 */
class ContractBuilder {
  /** Build the singleton instance of class
   * returns the existing instance if already created
   */
  static async build(contractClass, abi, contractAddress) {
    if (!!contractClass.instance) {
      return contractClass.instance;
    }

    const contractInstance = await createContractInstance(abi, contractAddress);
    const contractInstanceWS = await createContractInstanceWS(
      abi,
      contractAddress
    );

    contractClass.instance = new contractClass(
      abi,
      contractAddress,
      contractInstance,
      contractInstanceWS
    );

    return contractClass.instance;
  }
}

module.exports = ContractBuilder;
